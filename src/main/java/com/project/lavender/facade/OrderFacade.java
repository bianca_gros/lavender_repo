package com.project.lavender.facade;

import com.project.lavender.dao.InventoryDao;
import com.project.lavender.dao.OrderDao;
import com.project.lavender.model.Inventory;
import com.project.lavender.model.Orders;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Wrapper for the OrderDao service
 */
@AllArgsConstructor
@Component
public class OrderFacade {
    @Autowired
    private OrderDao daoO;
    @Autowired
    private InventoryDao daoI;

    /**
     * Places a given order and updates the stock for the ordered product
     * @param o the order to place
     * @return "Comanda a fost plasata cu succes!"
     */
    public String placeOrder(Orders o){
        daoO.save(o);
        Inventory i = findInventoryItem(o.getProduct_id());
        i.setQuantity(i.getQuantity() - o.getQuantity());
        daoI.save(i);
        return "Comanda a fost plasata cu succes!";
    }

    private Inventory findInventoryItem(int product_id){
        for(Inventory i : daoI.findAll()){
            if(i.getProduct_id() == product_id){
                return i;
            }
        }
        return null;
    }

    /**
     * Lists all orders
     * @return reference to the method findAll from DAO
     */
    public List<Orders> getOrders(){
        return (List<Orders>)daoO.findAll();
    }
}
