package com.project.lavender.facade;

import com.project.lavender.dao.CustomerDao;
import com.project.lavender.model.Customer;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Wrapper for the OrderDao service
 */
@AllArgsConstructor
@Component
public class CustomerFacade {
    @Autowired
    private CustomerDao daoC;

    /**
     * Adds a specific customer delegating the work to the DAO service
     * @param c the new customer
     * @return reference to the save method in DAO
     */
    public String addCustomer(Customer c){
        daoC.save(c);
        return "Client adaugat!";
    }

    /**
     * Lists all the existing customers
     * @return reference to the method findAllin DAO
     */
    public List<Customer> getCustomers(){
        return (List<Customer>)daoC.findAll();
    }

    public Customer getCustomerById(int id){
        for(Customer c : daoC.findAll()){
            if(c.getCustomer_id() == id){
                return c;
            }
        }
        return null;
    }

}
