package com.project.lavender.facade;

import com.project.lavender.dao.InventoryDao;
import com.project.lavender.dao.ProductDao;
import com.project.lavender.model.Inventory;
import com.project.lavender.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Wrapper for the ProductDao service
 */
@AllArgsConstructor
@Component
public class ProductFacade {
    @Autowired
    private ProductDao daoP;
    @Autowired
    private InventoryDao daoI;

    /**
     * Adds a specific product in the database
     * @param p the product to be added
     * @return "Produs adaugat!"
     */
    public String addProduct(Product p){
        daoP.save(p);
        daoI.save(new Inventory(p.getProduct_id(), 10));
        return "Produs adaugat!";
    }

    /**
     * Lists all existing products
     * @return reference to the method findAll from DAO
     */
    public List<Product> getProducts(){
        return (List<Product>)daoP.findAll();
    }

    /**
     * Deletes a specific product from database
     * @param productName the mane of the product to be deleted
     * @return "Produs sters!"
     */
    public String deleteProduct(String productName){
        final Product[] deSters = new Product[1];
        daoP.findAll().forEach(p ->{
            if(p.getProduct_name().equals(productName)){
                deSters[0] = p;
            }
                }
                );
        daoP.delete(deSters[0]);
        return "Produs sters!";
    }

    public String deleteAll(){
        daoP.deleteAll();
        return "Toate produsele au fost sterse!";
    }

    public Product getProductByName(String productName){
        for(Product p : daoP.findAll()){
            if(p.getProduct_name().equals(productName)){
                return p;
            }
        }
        return null;
    }

    public Product getProductById(int id){
        for(Product p : daoP.findAll()){
            if(p.getProduct_id() == id){
                return p;
            }
        }
        return null;
    }
}
