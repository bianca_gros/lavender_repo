package com.project.lavender.facade;

import com.project.lavender.dao.InventoryDao;
import com.project.lavender.model.Inventory;
import com.project.lavender.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Wrapper for the CustomerDao service
 */
@AllArgsConstructor
@Component
public class InventoryFacade {
    @Autowired
    InventoryDao daoI;

    /**
     * Lists all the existing inventory items
     * @return reference to the findAll method from DAO
     */
    public List<Inventory> getInventory(){
        return (List<Inventory>)daoI.findAll();
    }

    /**
     * Updates the stock for a specific inventory item
     * @param i the inventory item for which we want to update the stock
     * @return "Stocul a fost actualizat!"
     */
    public String updateStock (Inventory i){
        daoI.save(i);
        return "Stocul a fost actualizat!";
    }
}
