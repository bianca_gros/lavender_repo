package com.project.lavender.model;

import com.project.lavender.observer.MyObserver;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.*;

@SpringBootApplication
@Getter
@Setter
@ToString
@Entity
@EqualsAndHashCode
@Table(name = "customer")
/**
 * Represents the user of the PurpleLivingLavender webshop application
 * Its instance variables represents user data worth storing in the database
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14 */
public class Customer implements MyObserver {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int customer_id = 0;
    private String customer_name;
    private String email;
    private String password;

    /**
     * Update-like method from Observer Pattern which shows the change in the state of the observable object
     */
    @Override
    public void update() {
        System.out.println("Am fost notificat ca mi s-a inregistrat comanda (" + customer_name + ")");
    }
}
