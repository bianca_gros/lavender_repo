package com.project.lavender.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.*;

@SpringBootApplication
@Getter
@Setter
@ToString
@Entity
@EqualsAndHashCode
@Table(name = "inventory")
/**
 * Represents an extension of the product table, storing further information about a product, such as quantity
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public class Inventory {

    @Id
    private int product_id = 0;
    private int quantity;

    public Inventory(int id, int q) {
        this.product_id = id;
        this.quantity = q;
    }

    public Inventory() {

    }
}
