package com.project.lavender.model;

import com.project.lavender.business.MailingService;
import com.project.lavender.observer.MySubject;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

/**
 * Represents the orders admin of the system, which observs order placement and sends confirmation emails to customers who place orders
 */
@Component
public class Admin implements MySubject {

    private Customer c;
    private MailingService mail;

    public Admin(){
        this.c = new Customer();
        this.mail = new MailingService();
    }

    /**
     * Attaches a specific Object as an observer
     * @param o The Observer object
     */
    @Override
    public void setObserver(Object o) {
        this.c = (Customer)o;
    }

    /**
     * Notify-like method which updates the observer and sends confirmation emails
     */
    @Override
    public void notifyRegisteredOrder() {

        this.c.update();
        try {
            mail.sendConfirmationEmail(c.getEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
