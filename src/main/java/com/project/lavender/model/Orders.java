package com.project.lavender.model;

import lombok.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.*;

@SpringBootApplication
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@AllArgsConstructor
/**
 * Represents the orders placed on the webshop, storing further information about them, such as ordered product, placer customer, quantity etc.
 */
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int order_id = 0;
    private int customer_id;
    private int product_id;
    private String date;
    private int quantity;
    private float order_total;

    public Orders() {

    }
}
