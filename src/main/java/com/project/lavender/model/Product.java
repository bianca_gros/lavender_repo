package com.project.lavender.model;

import lombok.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.*;

@SpringBootApplication
@Getter
@Setter
@ToString
@Entity
@EqualsAndHashCode
@Table(name = "product")
/**
 * Class which represents the main table stored in the PurpleLivinglavender database - the product table.
 * Its instance variables represent table column names: identifier, name, description and price
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int product_id = 0;
    private String product_name;
    private String description;
    private float price;

}