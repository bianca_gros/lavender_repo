package com.project.lavender.controller;

import com.project.lavender.business.ProductBLL;
import com.project.lavender.model.Product;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Getter
@Setter
@RequestMapping("/product")
/**
 * Controller for {@link Product} table
 * Implements all the operations needed on products
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public class ProductController {
    @Autowired
    private ProductBLL pBLL;

    /**
     * Inserts a new product in the database
     * @param productName the name of the product
     * @param description the description for the present product
     * @param price the price of the product
     * @return "Produs adaugat" if the insertion was successfully
     */
    @PostMapping(path = "/addProduct")
    public @ResponseBody String addProduct(@RequestParam String productName, @RequestParam String description, @RequestParam float price){
        return pBLL.addProduct(productName,description, price);
    }

    /**
     * Lists the contents of the {@link Product} table
     * @return all the products stored in the database
     */
    @GetMapping(path = "/getProducts")
    public @ResponseBody List<Product> getProducts(){
        return pBLL.getProducts();
    }

    /**
     * Deletes by name a given product from the database
     * @param productName the name of the product to be deleted
     * @return "Produs sters" if the deletion was successfully, or "Produsul de sters nu a fost gasit!" if the product to be deleted was not found in the database
     */
    @DeleteMapping(path = "/deleteProduct")
    public @ResponseBody String deleteProduct(@RequestParam String productName){
        return pBLL.deleteProduct(productName);
    }

    /**
     * Integral deletion of the {@link Product} table
     * @return "Toate produsele au fost sterse!" after the successful deletion
     */
    @DeleteMapping(path = "/deleteAll")
    public @ResponseBody String deleteAll(){
        return pBLL.deleteAll();
    }


}
