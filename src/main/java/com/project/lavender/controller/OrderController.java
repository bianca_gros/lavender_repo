package com.project.lavender.controller;

import com.project.lavender.business.CustomerBLL;
import com.project.lavender.business.OrderBLL;
import com.project.lavender.model.Admin;
import com.project.lavender.model.Customer;
import com.project.lavender.model.Inventory;
import com.project.lavender.model.Orders;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for {@link Orders} table
 * Implements all the operations needed for managing the orders
 */
@RestController
@AllArgsConstructor
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    private OrderBLL oBLL;
    @Autowired
    private CustomerBLL cBLL;



    /**
     * Places a specific order
     * @param customer_id the id of the customer which places the order
     * @param product_id the id of the ordered product
     * @param date the date of the order
     * @param quantity the quantity ordered
     * @return reference to the placeOrder method from the corresponding BLL object
     */
    @PostMapping(path = "/placeOrder")
    public @ResponseBody
    String placeOrder(@RequestParam int customer_id, @RequestParam int product_id, @RequestParam String date, @RequestParam int quantity){

        return oBLL.placeOrder(customer_id, product_id, date, quantity);
    }

    /**
     * Lists all existing orders
     * @return a list of all the orders from the {@link Orders} table
     */
    @GetMapping(path = "/getOrders")
    public @ResponseBody
    List<Orders> getOrders(){
        return oBLL.getOrders();
    }
}
