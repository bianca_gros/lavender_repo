package com.project.lavender.controller;

import com.project.lavender.business.CustomerBLL;
import com.project.lavender.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
/**
 * Controller for {@link Customer} table
 * Implements all the operations needed on customers
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public class CustomerController {
    @Autowired
    private CustomerBLL cBLL;

    /**
     * Inserts a customer in the {@link Customer} table
     * @param name name of the client
     * @param email email address of the client
     * @param password password of the client
     * @return "Client adaugat!" if the insertion was successfully
     */
    @PostMapping(path = "/addCustomer")
    public @ResponseBody
    String addCustomer(@RequestParam String name, @RequestParam String email, @RequestParam String password){
        return cBLL.addCustomer(name, email, password);
    }

    /**
     * Lists the contents of the {@link Customer} table
     * @return the list of customers recorded in the database
     */
    @GetMapping(path = "/getCustomers")
    public @ResponseBody List<Customer> getCustomers(){
        return cBLL.getCustomers();
    }

}
