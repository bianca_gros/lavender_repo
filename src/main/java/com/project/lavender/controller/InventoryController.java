package com.project.lavender.controller;

import com.project.lavender.business.InventoryBLL;
import com.project.lavender.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inventory")
/**
 * Controller for {@link Inventory} table
 * Implements all the operations needed for managing the inventory
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public class InventoryController {

    @Autowired
    private InventoryBLL iBLL;

    /**
     * Lists the inventory
     * @return the list of {@link Inventory} items, with id and quantity
     */
    @GetMapping(path = "/getInventory")
    public @ResponseBody List<Inventory> getInventory(){
        return iBLL.getInventory();
    }

    /**
     * Updates the stock for a product given by name, with a specified quantity
     * @param productName the name of the product for which we want to update the stock
     * @param quantity either positive or negative integer
     * @return the new stock for the updated product, or "Produsul pentru care se doreste modificarea stocului nu exista!" if the product was not found
     */
    @PutMapping(path = "/updateStock")
    public @ResponseBody String updateStock (@RequestParam String productName, @RequestParam int quantity){
        return iBLL.updateStock(productName, quantity);
    }
}
