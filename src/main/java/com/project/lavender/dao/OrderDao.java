package com.project.lavender.dao;

import com.project.lavender.model.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Public repository used for making possible CRUD-like data manipulation in the {@link Orders} table
 */
@Repository
public interface OrderDao extends CrudRepository<Orders, Integer> {
}
