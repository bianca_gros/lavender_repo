package com.project.lavender.dao;

import com.project.lavender.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
/**
 * Public repository used for making possible CRUD-like data manipulation in the {@link Product} table
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public interface ProductDao extends CrudRepository<Product, Integer> {

}