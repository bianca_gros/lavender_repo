package com.project.lavender.dao;

import com.project.lavender.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
/**
 * Public repository used for making possible CRUD-like data manipulation in the {@link Customer} table
 *
 * @author Bianca Gros
 * @version 1.0
 * @since 2021-03-14
 */
public interface CustomerDao  extends CrudRepository<Customer, Integer> {
}
