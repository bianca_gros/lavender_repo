package com.project.lavender.business;

import com.project.lavender.facade.InventoryFacade;
import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.Customer;
import com.project.lavender.model.Inventory;
import com.project.lavender.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Class which encapsulates all the services needed for the operations on the {@link Inventory} table
 * Relies on a facade wrapping the DAO service
 */
@AllArgsConstructor
@Component
public class InventoryBLL {
    @Autowired
    InventoryFacade facadeI;
    @Autowired
    ProductFacade facadeP;

    /**
     * The list of all the lines in the {@link Inventory}
     * @return a list of all inventory items
     */
    public List<Inventory> getInventory(){
        return facadeI.getInventory();
    }

    /**
     * Updates the stock for a product given by name, with the specified quantity
     * @param productName the name of the product for which to update the stock
     * @param quantity the quantity by which to update the stock
     * @return reference to the updateStock method in the facade
     */
    public String updateStock (String productName, int quantity){
        List<Product> products = facadeP.getProducts();
        for(Product p : products){
            if(p.getProduct_name().equals(productName)){
                Inventory i = getItemById(p.getProduct_id());
                int newQuantity = i.getQuantity() + quantity;
                i.setQuantity(newQuantity);
                return facadeI.updateStock(i);
            }
        }
        return "Produsul pentru care se doreste modificarea stocului nu exista!";
    }

    private Inventory getItemById(int id){
        List<Inventory> items = facadeI.getInventory();
        for(Inventory i : items){
            if(i.getProduct_id() == id){
               return i;
            }
        }
        return null;
    }
}
