package com.project.lavender.business;

import com.project.lavender.facade.CustomerFacade;
import com.project.lavender.model.Customer;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Class which encapsulates all the services needed for the operations on the {@link Customer} table
 * Relies on a facade wrapping the DAO service
 */
@AllArgsConstructor
@Component
public class CustomerBLL {
    @Autowired
    private CustomerFacade facade;

    /**
     * Adds a new entry in the {@link Customer} table
     * @param name name of the new Customer
     * @param email email address of the new customer
     * @param password password of the new customer
     * @return reference to the addCustomer method in the facade
     */
    public String addCustomer(String name, String email, String password){
        Customer c = new Customer();
        c.setCustomer_name(name);
        c.setEmail(email);
        c.setPassword(password);
        return facade.addCustomer(c);
    }

    /**
     * The list of all the lines in the {@link Customer}
     * @return a list of all customers
     */
    public List<Customer> getCustomers(){
        return facade.getCustomers();
    }

    /**
     * Gets a customer by id
     * @param id the provided identifier for the customer
     * @return the found customer
     */
    public Customer getCustomerbyId(int id){
        for(Customer c : facade.getCustomers()){
            if(c.getCustomer_id() == id){
                return c;
            }
        }
        return null;
    }
}
