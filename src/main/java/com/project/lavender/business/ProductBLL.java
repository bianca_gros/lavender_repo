package com.project.lavender.business;

import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.Customer;
import com.project.lavender.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Class which encapsulates all the services needed for the operations on the {@link Product} table
 * Relies on a facade wrapping the DAO service
 */
@AllArgsConstructor
@Component
public class ProductBLL {
    @Autowired
    ProductFacade facade;

    /**
     * Adds a product in the {@link Product} table
     * @param productName the name of the new product
     * @param description the description of the new product
     * @param price the price for the new product
     * @return reference to the addProduct method in the facade
     */
    public String addProduct(String productName, String description, float price){
        Product p = new Product();
        p.setProduct_name(productName);
        p.setDescription(description);
        p.setPrice(price);
        return facade.addProduct(p);
    }

    /**
     * Lists all the existing products
     * @return a list of all products
     */
    public List<Product> getProducts(){
        return facade.getProducts();
    }

    /**
     * Deletes a specific product from the {@link Product} table
     * @param productName the name of the product to be deleted
     * @return reference to deleteProduct method in the facade
     */
    public String deleteProduct(String productName){
        return facade.deleteProduct(productName);

    }

    /**
     * Deletes all products from the database
     * @return reference to the method deleteAll in the facade
     */
    public String deleteAll(){
        return facade.deleteAll();
    }
}
