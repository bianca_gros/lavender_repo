package com.project.lavender.business;

import com.project.lavender.strategy.BillingStrategy;
import com.project.lavender.strategy.DiscountBilling;
import com.project.lavender.strategy.SimpleBilling;
import com.project.lavender.facade.CustomerFacade;
import com.project.lavender.facade.InventoryFacade;
import com.project.lavender.facade.OrderFacade;
import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Class which encapsulates all the services needed for the operations on the {@link Orders} table
 * Relies on a facade wrapping the DAO service
 */
@AllArgsConstructor
@Component
public class OrderBLL {
    @Autowired
    OrderFacade facadeO;
    @Autowired
    ProductFacade facadeP;
    @Autowired
    InventoryFacade facadeI;
    @Autowired
    CustomerFacade facadeC;
    private Admin a;
    private BillingStrategy bill;

    /**
     * Places a specific order
     * @param customer_id the id of the customer which places the order
     * @param product_id the id of the ordered product
     * @param date th date of the order
     * @param quantity the ordered quantity
     * @return reference to the placeOrder method in the facade
     */
    public String placeOrder(int customer_id, int product_id, String date, int quantity){


        Customer c = facadeC.getCustomerById(customer_id);


        Product p = getProduct(product_id);
        Inventory i = getInventoryItem(product_id);
        //if(i.getQuantity() >= quantity){
            Orders o = new Orders();
            o.setCustomer_id(customer_id);
            o.setProduct_id(product_id);
            o.setDate(date);
            o.setQuantity(quantity);
            float price = 0;
            if(p != null){
                price = p.getPrice();
            }
            //float total = quantity * price;
            float total = calculateOrderTotal(quantity, price);
            o.setOrder_total(total);
            //a.setObserver(c);
            //a.notifyRegisteredOrder();

            return facadeO.placeOrder(o);
        //}
        /*else{
            return "Stoc insuficient!";
        }*/
    }

    public Product getProduct(int product_id){
        return facadeP.getProductById(product_id);
    }

    private Inventory getInventoryItem(int product_id){
        for(Inventory i : facadeI.getInventory()){
            if(i.getProduct_id() == product_id){
                return i;
            }
        }
        return null;
    }

    /**
     * Method which calculates an order's total, based on the strategy to be applied
     * @param quantity the ordered quantity
     * @param price the price of the item
     * @return the total for an order
     */
    public float calculateOrderTotal(int quantity, float price){
        if(quantity >= 10){
            bill = new DiscountBilling();
        }else{
            bill = new SimpleBilling();
        }
        return bill.calculateOrderTotal(quantity, price);
    }

    /**
     * Lists all orders
     * @return a list of all orders from the {@link Orders} table
     */
    public List<Orders> getOrders(){
        return facadeO.getOrders();
    }
}
