package com.project.lavender.business;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Class which encapsulates the mailing services
 */
public class MailingService {

    /**
     * Send a typical email to the specified email address
     * @param emailAddress the recipient of the email
     * @throws MessagingException
     */
    public void sendConfirmationEmail(String emailAddress) throws MessagingException {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.mail.yahoo.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.ssl.trust", "smtp.mail.yahoo.com");

        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("grosbianca@yahoo.com", "uddebrpqjiexashv");
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("grosbianca@yahoo.com"));
        message.setRecipients(
                Message.RecipientType.TO, InternetAddress.parse(emailAddress));
        message.setSubject("Confirmare comanda Purple Living Lavender");

        message.setText("Comanda dumneavoastra a fost inregistrata. Va multumim!");

        Transport.send(message);
    }
}
