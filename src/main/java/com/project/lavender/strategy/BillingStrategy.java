package com.project.lavender.strategy;

/**
 * Public interface which defines the contract for billing and helps with Strategy pattern
 */
public interface BillingStrategy {
    /**
     * Calculates the order total for the ordered quantity and the given price
     * @param quantity the amount ordered
     * @param price the price of the ordered product
     * @return total for the order
     */
    float calculateOrderTotal(final int quantity, final float price);
}
