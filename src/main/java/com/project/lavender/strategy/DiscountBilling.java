package com.project.lavender.strategy;

/**
 * Class which defines the Discount Strategy for billing
 */
public class DiscountBilling implements BillingStrategy {
    /**
     * Calculates order total with discount (applied only for quantity greater or equal to 10)
     * @param quantity the amount ordered
     * @param price the price of the ordered product
     * @return the price for order after discount (final price)
     */
    @Override
    public float calculateOrderTotal(int quantity, float price) {
        return (float) ((price - 0.25 * price) * quantity);
    }
}
