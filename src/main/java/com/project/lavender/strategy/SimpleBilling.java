package com.project.lavender.strategy;

/**
 * Class which defines the Simple Strategy for billing
 */
public class SimpleBilling implements BillingStrategy {
    /**
     * Calculates order total without discount (applied only for quantity less than 10)
     * @param quantity the amount ordered
     * @param price the price of the ordered product
     * @return the final price for order (no discount applied)
     */
    @Override
    public float calculateOrderTotal(int quantity, float price) {
        return (quantity * price);
    }
}
