package com.project.lavender.observer;

/**
 * Interface which encapsulates methods setObserver and notifyRegisteredOrder which is implemented by Observable-like objects
 */
public interface MySubject {
    /**
     * Assigns an Observer object to the present Observable object
     * @param o the Observer to be assigned
     */
    public void setObserver(Object o);

    /**
     * Notify-like method which updates the observer and sends confirmation emails
     * Implemented by Observable objects
     */
    public void notifyRegisteredOrder();
}
