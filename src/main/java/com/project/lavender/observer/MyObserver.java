package com.project.lavender.observer;

/**
 * Interface which encapsulates method update which is implemented by Observer-like objects
 */
public interface MyObserver {

    /**
     * Updates the state of an Observer
     */
    public void update();
}
