package com.project.lavender;

import com.project.lavender.business.CustomerBLL;
import com.project.lavender.business.OrderBLL;
import com.project.lavender.business.ProductBLL;
import com.project.lavender.facade.CustomerFacade;
import com.project.lavender.facade.InventoryFacade;
import com.project.lavender.facade.OrderFacade;
import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.Admin;
import com.project.lavender.model.Customer;
import com.project.lavender.model.Product;
import com.project.lavender.strategy.BillingStrategy;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class LavenderApplicationTests {

    @Test
    void contextLoads() {
    }

    @Mock
    private Admin a;
    @Mock
    private ProductFacade pFacade;
    @Mock
    private InventoryFacade iFacade;
    @Mock
    private OrderFacade oFacade;
    @Mock
    private CustomerFacade cFacade;
    @Mock
    private Customer c;
    @Mock
    private BillingStrategy bill;

    @Before
    public void initialize(){
        this.a = Mockito.mock(Admin.class);
        this.c = Mockito.mock(Customer.class);
        this.pFacade = Mockito.mock(ProductFacade.class);
        this.cFacade = Mockito.mock(CustomerFacade.class);
        this.oFacade = Mockito.mock(OrderFacade.class);
        this.iFacade = Mockito.mock(InventoryFacade.class);
        this.bill = Mockito.mock(BillingStrategy.class);
    }

    @Test
    public void observerTest(){
        Admin a = new Admin();
        c.setCustomer_name("Client");
        c.setPassword("_client");
        c.setEmail("grosbianca@yahoo.com");
        a.setObserver(c);
        a.notifyRegisteredOrder();
        verify(c, times(1)).update();
    }

    @Test
    public void observerTest2(){
        this.initialize();
        OrderBLL oBLL = new OrderBLL(oFacade, pFacade, iFacade, cFacade, a, bill);
        CustomerBLL cBLL = new CustomerBLL(cFacade);
        ProductBLL pBLL = new ProductBLL(pFacade);

        cBLL.addCustomer(c.getCustomer_name(), c.getEmail(), c.getPassword());

        Product p = new Product();
        p.setProduct_name("lavanda");
        p.setPrice(10);
        p.setDescription("lavanda");
        pBLL.addProduct(p.getProduct_name(), p.getDescription(), p.getPrice());
        oBLL.placeOrder(c.getCustomer_id(), p.getProduct_id(), "03/03/2021", 1);
        verify(a,times(1)).notifyRegisteredOrder();
    }

    @Test
    public void strategyTest(){
        this.initialize();
        OrderBLL oBLL = new OrderBLL(oFacade, pFacade, iFacade, cFacade, a, bill);
        oBLL.calculateOrderTotal(11, 10);
        verify(bill, times(1)).calculateOrderTotal(11, 10);
    }

}
