package com.project.lavender.modelTesting;

import com.project.lavender.business.CustomerBLL;
import com.project.lavender.dao.CustomerDao;
import com.project.lavender.facade.CustomerFacade;
import com.project.lavender.model.Customer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTests {

    @Test
    void contextLoads() {
    }

    @Mock
    private CustomerFacade cFacade;

    @Test
    public void getCustomersTest(){
        assertNotNull(cFacade);
        CustomerBLL cBLL = new CustomerBLL(cFacade);
        cBLL.getCustomers();
        verify(cFacade, times(1)).getCustomers();
    }

    @Test
    public void addCustomerTest(){
        assertNotNull(cFacade);
        Customer c = new Customer();
        c.setCustomer_name("Gavris Vancea");
        c.setEmail("vancea_gavris@yahoo.com");
        c.setPassword("_gavris");
        CustomerBLL cBLL = new CustomerBLL(cFacade);
        cBLL.addCustomer(c.getCustomer_name(), c.getEmail(), c.getPassword());
        verify(cFacade, times(1)).addCustomer(c);
    }
}
