package com.project.lavender.modelTesting;

import com.project.lavender.business.ProductBLL;
import com.project.lavender.dao.InventoryDao;
import com.project.lavender.dao.ProductDao;
import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTests {

    @Test
    void contextLoads() {
    }

    @Mock
    private ProductFacade pFacade;
    @Mock
    private Product p = mock(Product.class);

    @Test
    public void getProductsTest (){
        assertNotNull(pFacade);
        ProductBLL pBLL = new ProductBLL(pFacade);
        pBLL.getProducts();
        verify(pFacade, times(1)).getProducts();
    }

    @Test
    public void addProductTest(){
        assertNotNull(pFacade);

        Product p = new Product();
        p.setProduct_name("lavanda");
        p.setPrice((float)10.0);
        p.setDescription("Lavandaaa!");

        ProductBLL pBLL = new ProductBLL(pFacade);
        pBLL.addProduct(p.getProduct_name(), p.getDescription(), p.getPrice());
        verify(pFacade, times(1)).addProduct(p);
    }

    @Test
    public void deleteProductTest(){
        assertNotNull(pFacade);
        ProductBLL pBLL = new ProductBLL(pFacade);

        Product p = new Product();
        p.setProduct_name("lavanda2");
        p.setPrice(11);
        p.setDescription("Lavandaaa2!");
        pBLL.deleteProduct(p.getProduct_name());

        verify(pFacade, times(1)).deleteProduct(p.getProduct_name());
    }

    @Test
    public void deleteAllTest(){
        assertNotNull(pFacade);
        ProductBLL pBLL = new ProductBLL(pFacade);
        pBLL.deleteAll();
        verify(pFacade, times(1)).deleteAll();
    }
}
