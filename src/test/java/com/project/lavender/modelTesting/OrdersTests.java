package com.project.lavender.modelTesting;

import com.project.lavender.strategy.BillingStrategy;
import com.project.lavender.business.OrderBLL;
import com.project.lavender.facade.CustomerFacade;
import com.project.lavender.facade.InventoryFacade;
import com.project.lavender.facade.OrderFacade;
import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrdersTests {

    @Test
    void contextLoads() {
    }

    @Mock
    private ProductFacade pFacade = mock(ProductFacade.class);
    @Mock
    private InventoryFacade iFacade = mock(InventoryFacade.class);
    @Mock
    private OrderFacade oFacade = mock(OrderFacade.class);
    @Mock
    private CustomerFacade cFacade = mock(CustomerFacade.class);
    @Mock
    private BillingStrategy bill = mock(BillingStrategy.class);
    /*@Mock
    private Product p;
    @Mock
    private Inventory i;
    @Mock
    private Customer c;*/

    @Test
    public void getOrdersTest(){
        assertNotNull(pFacade);
        assertNotNull(oFacade);
        assertNotNull(iFacade);
        OrderBLL oBLL = new OrderBLL(oFacade, pFacade, iFacade, cFacade, new Admin(), bill);
        oBLL.getOrders();
        verify(oFacade, times(1)).getOrders();
    }

    @Test
    public void placeOrderTest(){
        assertNotNull(pFacade);
        assertNotNull(oFacade);
        assertNotNull(iFacade);
        OrderBLL oBLL = new OrderBLL(oFacade, pFacade, iFacade, cFacade, new Admin(), bill);

        Product p = new Product();
        p.setProduct_id(0);
        p.setDescription("Lavandaaa!");
        p.setProduct_name("lavanda");
        p.setPrice(10);

        Inventory i = new Inventory();
        i.setProduct_id(0);
        i.setQuantity(100);

        Customer c = new Customer();
        c.setCustomer_name("Gavris Vancea");
        c.setEmail("vancea_gavris@yahoo.com");
        c.setPassword("_gavris");

        Orders o = new Orders();
        o.setCustomer_id(c.getCustomer_id());
        o.setProduct_id(p.getProduct_id());
        o.setDate("27/03/20");
        o.setQuantity(1);
        float price = 10;
        float total = 1 * price;
        o.setOrder_total(total);

        oBLL.placeOrder(c.getCustomer_id(), p.getProduct_id(), o.getDate(), o.getQuantity());
        verify(oFacade, times(1)).placeOrder(o);
    }



}
