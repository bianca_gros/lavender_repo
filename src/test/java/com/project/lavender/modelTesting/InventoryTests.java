package com.project.lavender.modelTesting;

import com.project.lavender.business.InventoryBLL;
import com.project.lavender.business.ProductBLL;
import com.project.lavender.dao.InventoryDao;
import com.project.lavender.dao.ProductDao;
import com.project.lavender.facade.InventoryFacade;
import com.project.lavender.facade.ProductFacade;
import com.project.lavender.model.Inventory;
import com.project.lavender.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryTests {

    @Test
    void contextLoads() {
    }

    @Mock
    private ProductFacade pFacade = mock(ProductFacade.class);
    @Mock
    private InventoryFacade iFacade = mock(InventoryFacade.class);
    /*@Mock
    private ProductBLL pBLL = mock(ProductBLL.class);*/

    @Test
    public void getInventoryTest(){
        assertNotNull(iFacade);
        assertNotNull(pFacade);
        InventoryBLL iBLL = new InventoryBLL(iFacade, pFacade);
        iBLL.getInventory();
        verify(iFacade,times(1)).getInventory();
    }

    @Test
    public void updateStockTest(){

        assertNotNull(iFacade);
        assertNotNull(pFacade);
        InventoryBLL iBLL = new InventoryBLL(iFacade, pFacade);

        Product p = new Product();
        //p.setProduct_id(0);
        p.setDescription("Lavandaaa!");
        p.setProduct_name("lavanda");
        p.setPrice(10);

        ProductBLL pBLL = new ProductBLL(pFacade);
        pBLL.addProduct(p.getProduct_name(), p.getDescription(), p.getPrice());
        verify(pFacade, times(1)).addProduct(p);


        Inventory i = new Inventory();
        i.setProduct_id(p.getProduct_id());
        i.setQuantity(10);

        iBLL.updateStock(p.getProduct_name(), 100);
        verify(iFacade, times(1)).updateStock(i);


    }
}
