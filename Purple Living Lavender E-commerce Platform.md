# Purple Living Lavender E-commerce Platform
_Dupa a treia iteratie a proiectului_

# Obiective
### Obiectiv general

Realizarea unei platforme RESTful web-based pentru o mica "fabrica de mov" - Purple Living Lavender care include si un magazin online pentru comercializarea de obiecte naturale si artizanale din lavanda.

### Obiective secundare

- Magazin online
- Creare cont utilizator
- Crearea unei pagini de "Bun venit"
- Pagina "Despre noi"
- Pagina "Contact"
- To be continued...

> Aplicatia este scrisa in Java si respecta arhitectura REST. 

> Utilizeaza framework-ul open-source Spring (varianta Spring Boot) 
si este construita pe modelul MVC. 

> Sistemul de gestiune de 
baze de date utilizat este MySQL.

> Intreaga aplicatie este testata prin Unit Testing cu 
ajutorul framework-ului de testare JUnit.


# Cerinte

### Cerinte functionale
- Accesarea magazinului online
- Vizualizare produse in diferite moduri
- Creare cont utilizator
- Plasare de comenzi

### Cerinte nonfunctionale
- Timpul de raspuns relativ scurt
- Recuperare buna din erori
- Usurinta in mentenabilitate
- Reutilizabilitate intr-o eventuala dorinta de dezvoltare
- Costuri mici de livrare
- Portabilitate
- User-friendly

# Magazin online

Vor exista mai multe secțiuni ale acestui site, printre care și un magazine online. Magazinul va putea fi accesat de utilizatori prin Internet, fiind găzduit online, iar comenzile se vor putea plasa pe site pe baza unui cont de utilizator create în prealabil. Pe baza contului create de utilizator, se va crea automat și un profil al acestuia, cu secțiune de produse favorite, coș de cumpărături, preferințe legate de transport și plată etc. Utilizatorii vor avea libertatea de a-și personaliza informațiile din contul personal oricând.
Produsele vor fi grupate pe categorii, vizualizarea lor se va putea face în mai multe moduri, în mod sortat sau pe baza unor recomandări, cu detalii sau succint. Toate detaliile despre un produs vor fi vizibile (nume, descriere, preț etc.). 


# Starea actuala a proiectului - tema 1
Ca prima iteratie a proiectului, am reusit crearea scheletului Spring Boot pe model MVC care va sustine platforma de e-commerce si am realizat conectarea la baza de date.

Aplicatia este structurata pe 3 pachete utilitare si un pachet cu clasa de testare: model, dao si controller, impreuna cu o clasa la acelasi nivel cu pachetele. Pachetul model include clasele care se identifica cu tabelele din baza de date: Product, Customer si Inventory. Pachetul dao cuprinde trei interfete, fiecare corespunzand unei clase din pachetul model si fiecare extinzand CrudRepository. Aceste interfete fac posibil accesul simplificat la operatiile de tip CRUD. Pachetul controller cuprinde cate un controller pentru fiecare clasa din pachetul model si controleaza operatiile CRUD specifice fiecarei entitati din model.
Clasa de test foloseste Mockito pentru a simula si testa accesele la baza de date si operatiile care se pot face. S-a creat cate o metoda de test pentru fiecare metoda utilitara din clasele apartinand pachetului controller.
De asemenea, proiectul are realizata deocumentatia javaDoc si este incarcat pe git, unde se pot vedea fazele dezvoltarii proiectului, corespunzand commit-urilor multiple.

# Starea actuala a proiectului - tema 2
Lucrul pentru al doilea increment al proiectului a adus cateva modificari in ceea ce priveste structura aplicatiei. Noua structurare pe pachete este:
- pachetul model: contine clasele ce fac parte din domeniul problemei (Product, Orders, Customer, Inventory, Admin)
- pachetul business: contine clasele care realizeaza logica de business a aplicatiei (ProductBLL, OrderBLL, CustomerBLL, InventoryBLL)
- pachetul dao: contine clasele care faciliteaza accesul la baza de date = repositories (ProductDao, OrderDao, CustomerDao, InventoryDao)
- pachetul facade: contine clasele de tip wrapper care se interpun intre repositories si BLL
- pachetul controller: contine clasele care coordoneaza realizarea endpoint-urilor - CustomerController, ProductController, OrderController, InventoryController
- pachetul observer - reuneste doua interfete necesare pentru implementarea Design Pattern-ului Observer (MySubject si MyObserver, implementate de clasele Admin, repsectiv de Customer)


### Lista endpoint-urilor (implementate pentru temele 1 si 2):
- POST: localhost:8080/customer/addCustomer
- GET: localhost:8080/customer/getCustomers
- GET: localhost:8080/inventory/getInventory
- PUT: localhost:8080/inventory/updateStock
- POST: localhost:8080/orders/placeOrder
- GET: localhost:8080/orders/getOrders
- POST: localhost:8080/product/addProduct
- GET: localhost:8080/product/getProducts
- DELETE:localhost:8080/product/deleteProduct
- DELETE: localhost:8080/product/deleteAll

> Pentru fiecare metoda ce implementeaza endpoint-uri au fost create metode de test utilizand JUnit si Mockito, reunind endpoint-urile ce se refera la acelasi tabel in acceasi clasa de test

### Observer Design Pattern
Pentru aceasta iteratie a proiectului am adaugat si design pattern-ul Observer.
Acesta se materializeaza prin obiectele Admin si Customer care implementeaza interfetele MySubject respectiv MyObserver. Admin-ul are responsabilitatea de a valida comanda plasata de Customer-ul care a plasat-o. Atfel, Customer-ul va fi notificat: va afisa acest lucru la consola, dar va primi si un mail in care este anuntat ca i s-a validat plasarea comenzii (doar adrese de mail yahoo).

### Diagrame - iteratia 2
Mai jos sunt prezentate cele doua diagrame realizate in aceasta iteratie a proiectului. Este vorba despre diagrama UML de clase a pattern-ului Observer si despre diagrama de deployment

- Diagrama UML de clase pentru design pattern-ul Observer prezinta cele doua interfete necesare - MySubject si MyObserver - precum si clasele ce implementeaza aceste interfete si indeplinesc rolurile introduse de acestea.

![Observer Design Pattern - UML Diagram](observer_diagram.png)

- Diagrama de deployment a aplicatiei prezinta arhitectura de executie pe care va rula sistemul. Are 3 elemente centrale: Serverul Sping Boot in spatele caruia ruleaza un server Apache Tomcat care permite comunicarea cu Clientul prin protocolul HTTP. Clientul plaseaza cereri (requests) catre Server si primeste raspunsuri (responses) de la acesta. Pentru indeplinirea acestor cereri, Serverul este conectat la o baza de date de tip MySQL Server care stocheaza informatiile necesare functionarii intregii aplicatii.

![Purple Living lavender Application - Deployment Diagram](deployment_diagram.png)

# Starea actuala a proiectului - tema 3
Lucrul pentru al treilea increment al proiectului s-a materializat in implementarea Design Pattern-ului Strategy, un pattern comportamental ce permite alegerea dinamica a algoritmului dorit, la runtime. Datorita acestei alegeri dinamice, codul creste in flexibilitate si reutilizabilitate. De asemenea, clientul (apelantul) nu stie si nu trebuie sa stie care dintre algoritmi a fost ales, fapt ce sporeste incapsularea ca principiu OOP.

### Strategy Design Pattern
Am decis ca implementarea pattern-ului Strategy se potriveste cel mai natural la calculul totalului unei comenzi. Politica webshop-ului este ca atunci cand un client cumpara mai mult de 10 bucati dintr-un produs intr-o singura comanda, beneficiaza de o reducere de 25%. Daca numarul de produse comandate este sub 10, se aplica facturarea cu pret intreg.

Astfel, am descris interfata BillingStrategy, care contine doar metoda calculateOrderTotal, si doua clase concrete: SimpleBilling si DiscountBilling, ambele implementand interfata descrisa mai sus prin implementarea metodei abstracte calculateOrderTotal. Clasa SimpleBilling calculeaza totalul unei comenzi la pretul intreg al produselor, fara a aplica reducerea. Clasa DiscountBilling insa, calculeaza totalul comenzii aplicand reducerea de 25%. 

Pentru aplicarea pattern-ului, in clasa responsabila de logica comenzilor, OrderBLL, am adaugat o variabila instanta de tip BillingStrategy, pe care am instantiat-o doar in metoda calculateOrderTotal, in functie de numarul de produse comandate, ori la SimpleBilling, ori la DiscountBilling. In metoda placeOrder tot din clasa OrderBLL am calculat totalul comenzii prin apelul metodei calculateOrderTotal din clasa corespunzatoare clasei la care s-a instantiat variabila instanta de tip BillingStrategy.

### Diagrame - iteratia 3
Mai jos este prezentata diagrama realizata in aceasta iteratie a proiectului. Este vorba despre diagrama de secventa pentru use case-ul "Place Order":

![Sequence Diagram - Place Order use case](SequenceDiagram.png)

- In use case-ul "Place Order", actorul se adreseaza OrderBLL, apeland metoda placeOrder. Deoarece este nevoie de clientul care pleaseaza comanda, OrderBLL cheama CustomerFacade prin metoda getCustomerById. Este nevoie si de produsul comandat, asa ca se cheama ProductFacade prin metoda getProductById ce returneaza un produs. Se gaseste si numarul de inventar al acelui produs, apoi se creeaza o comanda noua. Trebuie sa se calculeze totalul pentru comanda, moment in care se apeleaza calculateOrderTotal din OrderBLL, care cheama calculateOrderTotal din DiscountBilling sau SimpleBilling, in fucntie de in care caz al pattern-ului Strategy ne gasim. Abia apoi se plaseaza comanda prin metoda de placeOrder din OrderFacade.






